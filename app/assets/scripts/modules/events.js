import $ from 'jquery';

$(document).ready(() => {

  $('.menu-toggle').on('click', () => {
    if ($('.menu-toggle').hasClass('menu-closed')) {
      $('.menu-toggle').removeClass('menu-closed');
      $('.menu-toggle').addClass('menu-open');
      $('.menu-links').css({'display': 'block'});
      $('html').css({'overflow': 'hidden'});
    } else {
      $('.menu-toggle').addClass('menu-closed');
      $('.menu-toggle').removeClass('menu-open');
      $('.menu-links').css({'display': ''});
      $('html').css({'overflow': ''});
    }
  });

});
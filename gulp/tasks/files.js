const gulp = require('gulp');
const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');
const browserSync = require('browser-sync');

gulp.task('page',function(){
    return gulp.src('app/views/**/*.html')
    .pipe(gulp.dest('web/'))
    .pipe(browserSync.reload({
        stream: true
    }));
});

gulp.task('fonts',function(){
    return gulp.src('app/assets/fonts/**/*')
    .pipe(gulp.dest('web/resources/fonts/'))
    .pipe(browserSync.reload({
        stream: true
    }));
});

gulp.task('images',function(){
    return gulp.src('app/assets/images/**/*')
    .pipe(imagemin({
        progressive: true,
        svgPlugins: [{
            removeViewBox: true
        }],
        use: [pngquant()],
        interlaced: true
    }))
    .pipe(gulp.dest('web/resources/images/'))
    .pipe(browserSync.reload({
        stream: true
    }));
});